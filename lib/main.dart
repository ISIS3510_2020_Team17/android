
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:motivaapp/app_router.dart';
import 'package:motivaapp/config/app_colors.dart';
import 'package:motivaapp/store.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:motivaapp/store/application_state.dart';
import 'package:motivaapp/store/google_user_state.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    Firebase.initializeApp();
    return StoreProvider<ApplicationState>(
      store: ApplicationStore.store,
      child: MaterialApp(
        title: 'Motivapp',
        theme: ThemeData(
          // This is the theme of your application.
          //
          // Try running your application with "flutter run". You'll see the
          // application has a blue toolbar. Then, without quitting the app, try
          // changing the primarySwatch below to Colors.green and then invoke
          // "hot reload" (press "r" in the console where you ran "flutter run",
          // or simply save your changes to "hot reload" in a Flutter IDE).
          // Notice that the counter didn't reset back to zero; the application
          // is not restarted.
          appBarTheme: AppBarTheme(
            color: AppColors.$primary,
          ),
          // This makes the visual density adapt to the platform that you run
          // the app on. For desktop platforms, the controls will be smaller and
          // closer together (more dense) than on mobile platforms.
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        onGenerateRoute: (settings) =>
            AppRouter.onGenerateRoute(settings, context),
      ),
    );
  }
}
