import 'package:motivaapp/services/storage_helper.dart';
import 'package:motivaapp/store/actions.dart';
import 'package:motivaapp/store/application_reducer.dart';
import 'package:motivaapp/store/application_state.dart';
import 'package:motivaapp/store/set_google_user_action.dart';
import 'package:motivaapp/store/google_user_reducer.dart';
import 'package:motivaapp/store/google_user_state.dart';
import 'package:redux/redux.dart';

class ApplicationStore {
  static bool _retrieveUser = true;
  static Store<ApplicationState> _store =
      Store<ApplicationState>
        (_appReducer, initialState: ApplicationState(null, null));

  static Store<ApplicationState> get store => _store;

  static ApplicationState _appReducer(ApplicationState prevState, dynamic action) {
    final red = applicationReducer(prevState, action);
    return red;
  }
}
