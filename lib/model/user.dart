import 'package:flutter/cupertino.dart';
import 'package:motivaapp/model/jsonifable.dart';

@immutable
class User extends Jsonifable {
  final String fullName;
  final String photoUrl;
  final String email;
  final String id;

  getId() => id;

  User(this.fullName, this.email, this.id, this.photoUrl);

  User.fromJson(Map<String, dynamic> json):
    this.fullName = json['fullName'],
    this.email = json['email'],
    this.photoUrl = json['photoUrl'],
    this.id = json['id'];

  Map<String, dynamic> toJson() {
    final json = Map<String, dynamic>();
    json['fullName'] = this.fullName;
    json['email'] = this.email;
    json['photoUrl'] = this.photoUrl;
    json['id'] = this.id;
    return json;
  }
  
}
