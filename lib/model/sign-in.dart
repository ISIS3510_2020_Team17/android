import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:motivaapp/adapter/sqlite.dart';
import 'package:motivaapp/components/base_component.dart';
import 'package:motivaapp/model/Sub.dart';
import 'package:motivaapp/model/user.dart';
import 'package:motivaapp/services/connectivity_check.dart';
import 'package:motivaapp/services/storage_helper.dart';
import 'package:motivaapp/store/set_google_user_action.dart';
import 'package:motivaapp/store/set_user_action.dart';
import 'package:motivaapp/store/user_state.dart';

import '../store.dart';

class SignIn extends Sub {
  GoogleSignIn _googlesignIn;

  static List<Sub> _listeners = [];

  static SignIn _signIn = SignIn();

  static DBAdapter<User> adapter;

  SignIn() {
    adapter = DBAdapter('user', this);
    adapter.initialize(unique: true);
    _googlesignIn = GoogleSignIn(
      scopes: <String>[
        'https://www.googleapis.com/auth/userinfo.profile',
        'email',
      ],
    );
    _googlesignIn.onCurrentUserChanged.listen((account) async => {
          await ApplicationStore.store.dispatch(SetGoogleUserAction(account)),
          for (final listener in _listeners) {
            listener.notify(account)
          }
        });
    // Lookup for internet connectivity
    checkConnectivity().then((result) => {
      if(result) {
        // All good}

      } else
        {
          // Get user from localStorage
          _getUserFromDatabase(),
        }
    });
  }

  static Future<void> _getUserFromDatabase() async{
    if(adapter.object != null) {
      ApplicationStore.store.dispatch(SetUserAction(adapter.object));
    }
    for( final listener in _listeners) {
      listener.notify(adapter.object);
    }
  }

  static _getUserFromStorage() async{

    final storage = StorageHelper();
    final user = await storage.getObject('user');
    if(user != null && user is Map<String, dynamic>) {
      User parsedUser = User.fromJson(user);
      ApplicationStore.store.dispatch(SetUserAction(parsedUser));
    }
    for(final listener in _listeners) {
      listener.notify(user);
    }
  }

  static GoogleSignIn get signIn => _signIn._googlesignIn;

  static addListener(Sub context) {
    _listeners.add(context);
  }

  static removeListener(Sub context) {
    _listeners.remove(context);
  }

  @override
  notify(user) {
    // TODO: implement notify
    if(ApplicationStore.store.state.userState == null && user is User) {
      ApplicationStore.store.dispatch(SetUserAction(user));
    }
    for(final listener in _listeners) {
      listener.notify(user);
    }
  }

  

}
