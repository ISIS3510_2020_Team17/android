abstract class Jsonifable {
  Map<String, dynamic> toJson();

  getId();
}