import 'package:motivaapp/model/user.dart';

class AppFactory {
  static T buildFromJson<T>(Map<String, dynamic> json) {
    if(T.toString() == 'User' || T is User) {
      return User.fromJson(json) as T;
    } else {
      throw StateError('No builder for $T found');
    }
  }
}