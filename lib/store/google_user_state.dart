import 'package:flutter/cupertino.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:motivaapp/services/storage_helper.dart';

@immutable
class GoogleUserState {
  final GoogleSignInAccount currentUser;

  GoogleUserState(this.currentUser);
}