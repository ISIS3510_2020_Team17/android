import 'package:flutter/cupertino.dart';
import 'package:motivaapp/store/application_state.dart';

@immutable
class SetApplicationAction {
  final ApplicationState applicationState;
  SetApplicationAction(this.applicationState);
}