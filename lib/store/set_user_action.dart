import 'package:flutter/cupertino.dart';
import 'package:motivaapp/model/user.dart';

@immutable
class SetUserAction {
  final User user;
  SetUserAction(this.user);
}