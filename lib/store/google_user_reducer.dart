import 'package:motivaapp/store/set_google_user_action.dart';
import 'package:motivaapp/store/google_user_state.dart';

googleUserReducer(GoogleUserState prevState, SetGoogleUserAction action) {
  return GoogleUserState(action.user);
}