import 'package:motivaapp/adapter/sqlite.dart';
import 'package:motivaapp/model/user.dart';
import 'package:motivaapp/services/storage_helper.dart';
import 'package:motivaapp/store.dart';
import 'package:motivaapp/store/application_state.dart';
import 'package:motivaapp/store/google_user_state.dart';
import 'package:motivaapp/store/set_application_action.dart';
import 'package:motivaapp/store/set_google_user_action.dart';
import 'package:motivaapp/store/set_user_action.dart';
import 'package:motivaapp/store/user_state.dart';

ApplicationState applicationReducer(ApplicationState prev, dynamic action) {
  if(action is SetGoogleUserAction) {
    if(action.user != null) {
      ApplicationStore.store.dispatch(SetUserAction(
          User(action.user.displayName, action.user.email,
              action.user.id, action.user.photoUrl)));
    } else {
      ApplicationStore.store.dispatch(SetUserAction(null));
    }
    return ApplicationState(GoogleUserState(action.user),
        ApplicationStore.store.state.userState);
  } else if(action is SetUserAction) {
    // Set user in JSON
    print(action.user);
    //final storage = StorageHelper();
    if(action.user == null) {
      //storage.deleteObject('user');
      storeUser() async {
        final adapter = DBAdapter<User>('user', null);
        await adapter.initialize(unique: true);
        if(adapter.object != null) {
          await adapter.deleteObject(adapter.object);
        }
      }
      storeUser();
    } else {
      storeUser() async{
        final adapter = DBAdapter<User>('user', null);
        await adapter.initialize(unique: true);
        await adapter.setObject(action.user);
      }
      storeUser();
    }
    return ApplicationState(ApplicationStore.store.state.googleUserState,
        UserState(action.user));
  }
  return prev;
}