import 'package:motivaapp/store/set_user_action.dart';
import 'package:motivaapp/store/user_state.dart';

UserReducer(UserState prevState, SetUserAction userAction) {
  return UserState(userAction.user);
}