import 'package:flutter/cupertino.dart';
import 'package:motivaapp/store/google_user_state.dart';
import 'package:motivaapp/store/user_state.dart';

@immutable
class ApplicationState {
  final GoogleUserState googleUserState;
  final UserState userState;

  ApplicationState(this.googleUserState, this.userState);
}