import 'package:flutter/cupertino.dart';
import 'package:motivaapp/model/user.dart';

@immutable
class UserState {
  final User user;

  UserState(this.user);

}
