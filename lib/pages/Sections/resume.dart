import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:motivaapp/components/gauge_pie.dart';
import 'package:motivaapp/config/fonts.dart';

class Resume extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return ResumeState();
  }
}

class ResumeState extends State<Resume> {
  @override
void initState() {
    // TODO: implement initState
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      await showDialog<void>(
        context: context,
        barrierDismissible: true, // user must tap button!
        builder: (BuildContext context) {
          return AlertDialog(
            content: SingleChildScrollView(
              child: ListBody(
                children: <Widget>[
                  Fonts.title('Motivapp'),
                  Container(height: 24),
                  Fonts.text(
                      'We know sometimes you have bad moments, if you wanna talk with someone, just shake your device'),
                ],
              ),
            ),
            actions: <Widget>[
              TextButton(
                child: Fonts.text('Ok'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        },
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    double width = MediaQuery.of(context).size.width;
    return (Container(
      padding: EdgeInsets.all(24),
      child: SingleChildScrollView(
        child: Column(
          children: [
            Fonts.title('You\'re almost there'),
            Container(
              height: 18,
            ),
            Fonts.subtitle(
                'Hi Julian, this is the summary of your daily tasks'),
            Container(
              height: 18,
            ),
            Container(
              child: Stack(children: [
                GaugeChart.withSampleData(),
                Center(
                  child: Fonts.title('60%'),
                )
              ]),
              width: width / 2,
              height: width / 2,
            ),
            Fonts.text(
                'Keep working on your daily goals, you are 3h away from finishing the tasks.'),
            Container(
              height: 18,
            ),
            Fonts.subtitle2('Your next task is'),
            Container(
              height: 18,
            ),
            Card(
              clipBehavior: Clip.antiAlias,
              child: Column(
                children: [
                  ListTile(
                    leading: Container(
                      child: Icon(Icons.assessment),
                      height: Size.infinite.height,
                    ),
                    title: Container(
                      child: Fonts.text('Check out balance'),
                      alignment: AlignmentDirectional.topStart,
                    ),
                    subtitle: Container(
                      child: Fonts.small('St. Jonas Company'),
                      alignment: AlignmentDirectional.centerStart,
                    ),
                    trailing: Fonts.text('5\nmin'),
                  ),
                  ListTile(
                    leading: Container(
                      child: Icon(Icons.assessment),
                      height: Size.infinite.height,
                    ),
                    title: Container(
                      child: Fonts.text('Appointment'),
                      alignment: AlignmentDirectional.topStart,
                    ),
                    subtitle: Container(
                      child: Fonts.small('Universidad de los Andes'),
                      alignment: AlignmentDirectional.centerStart,
                    ),
                    trailing: Fonts.text('2\nhrs'),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    ));
  }
}
