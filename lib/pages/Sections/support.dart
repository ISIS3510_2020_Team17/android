import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:motivaapp/config/fonts.dart';

class Support extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      padding: EdgeInsets.all(24),
      child: SingleChildScrollView(
        child: Column(
          children: [
            Fonts.title('Everyone needs a hand'),
            Container(height: 24),
            Fonts.subtitle('Have a talk with a professional. Our team is always waiting for you'),
            Container(height: 24,),
            Card(
              child: ListTile(
                title: Container(
                    child: Fonts.text('Agent Owl'),
                  alignment: AlignmentDirectional.topStart,
                ),
                subtitle: Container(
                  child: Fonts.small('Active now'),
                  alignment: AlignmentDirectional.bottomStart,
                ),
                onTap: () {
                  Navigator.pushNamed(context, '/chat/owly');
                },
              ),
            ),
          ],
        ),
      ),
    );
  }

}