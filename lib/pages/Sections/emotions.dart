import 'package:flutter/cupertino.dart';
import 'package:motivaapp/config/app_colors.dart';
import 'package:motivaapp/config/fonts.dart';
import 'package:table_calendar/table_calendar.dart';

class Emotions extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final calendarController = CalendarController();
    // TODO: implement build
    return Container(
      padding: EdgeInsets.all(24),
      child: SingleChildScrollView(
        child: Column(
          children: [
            Fonts.title('How do you feel today'),
            Container(height: 18),
            Fonts.subtitle(
                'We do care for your emotional status, here is the summary of the feelings you\'re feeling this month'),
            Container(height: 18),
            TableCalendar(
              calendarController: calendarController,
              initialCalendarFormat: CalendarFormat.month,
              availableCalendarFormats: {CalendarFormat.month: 'Month'},
              calendarStyle: CalendarStyle(
                todayColor: AppColors.$primary,
                selectedColor: AppColors.$secondary,
                markersColor: AppColors.$accent,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
