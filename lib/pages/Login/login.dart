import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:motivaapp/model/Sub.dart';
import 'package:motivaapp/model/sign-in.dart';
import 'package:motivaapp/store.dart';
import 'package:motivaapp/store/set_google_user_action.dart';

class Login extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return LoginState();
  }
}

class LoginState extends State<Login> implements Sub {

  @override
  notify(account) {
    if(account != null) {
      if(mounted) {
        Navigator.pushReplacementNamed(this._context, '/home');
      }
    }
  }

  BuildContext _context;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    SignIn.addListener(this);
  }

  @override
  void dispose() {
    // TODO: implement dispose
    SignIn.removeListener(this);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    this._context = context;
    //SignIn.addListener(this);
    GoogleSignIn _googleSignIn = SignIn.signIn;
    _googleSignIn.signInSilently();
    // TODO: implement build
    return Scaffold(
      //appBar: DefaultAppBar(context, title: ''),
      body: Container(
        alignment: Alignment.center,
        child: SingleChildScrollView(
          scrollDirection: Axis.vertical,
          child: Column(
            //mainAxisAlignment: MainAxisAlignment.center,
            //crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              Image(
                image: AssetImage('images/logo.png'),
              ),
              OutlineButton.icon(
                icon: Image.asset('images/google_logo.png'),
                label: Text('Log in with Google'),
                onPressed: () => {_googleSignIn.signIn()},
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(25.0),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
