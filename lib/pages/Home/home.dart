import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:motivaapp/components/app_bar.dart';
import 'package:motivaapp/components/drawer.dart';
import 'package:motivaapp/components/gauge_pie.dart';
import 'package:motivaapp/config/app_colors.dart';
import 'package:motivaapp/config/fonts.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:motivaapp/pages/Sections/emotions.dart';
import 'package:motivaapp/pages/Sections/resume.dart';
import 'package:motivaapp/pages/Sections/support.dart';
import 'package:shake/shake.dart';

class Home extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return HomeState();
  }
}

class HomeState extends State<Home> {
  int _index = 0;
  ShakeDetector _detector;


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _detector = ShakeDetector.autoStart(
        onPhoneShake: () {
          // Do stuff on phone shake
          print('f');
          Navigator.pushNamed(context, '/chat/owly');
        }
    );
  }



  @override
  void dispose() {
    // TODO: implement dispose
    _detector.stopListening();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    //Firebase.initializeApp();
    double width = MediaQuery.of(context).size.width;
    // TODO: implement build
    return Scaffold(
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        backgroundColor: AppColors.$primary,
        currentIndex: _index,
        selectedItemColor: Colors.black,
        unselectedItemColor: Colors.black.withOpacity(.60),
        selectedFontSize: 14,
        unselectedFontSize: 14,
        onTap: (value) {
          // Respond to item press.
          setState(() => _index = value);
        },
        items: [
          BottomNavigationBarItem(
            label: 'Home',
            icon: Icon(Icons.home),
          ),
          BottomNavigationBarItem(
            label: 'Calendar',
            icon: Icon(Icons.calendar_today),
          ),
          BottomNavigationBarItem(
            label: 'Support',
            icon: Icon(Icons.support),
          ),
        ],
      ),
      appBar: UserAppBar(context, title: 'Motivapp'),
      drawer: AppDrawer(context),
      body: getViewFromPos(_index),
    );
  }
}

getViewFromPos(int pos) {
  switch (pos) {
    case 0:
      return Resume();
    case 1:
      return Emotions();
    case 2:
      return Support();
  }
}
