import 'package:flutter/material.dart';

class AppColors {
  static final $primary = Colors.lightBlue[500];
  static final $accent = Colors.deepOrangeAccent[700];
  static final $secondary = Colors.amberAccent[700];

}