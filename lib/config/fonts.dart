import 'package:flutter/material.dart';

class Fonts {
  static title(text) {
    return Text(
      text,
      textAlign: TextAlign.center,
      style: TextStyle(
        color: Color.fromRGBO(112, 112, 112, 1),
        fontFamily: 'Segoe UI',
        fontFamilyFallback: ['roboto', 'sans-serif'],
        fontSize: 32,
      ),
    );
  }

  static subtitle(text) {
    return Text(
      text,
      textAlign: TextAlign.center,
      style: TextStyle(
        color: Color.fromRGBO(112, 112, 112, 1),
        fontFamily: 'Segoe UI',
        fontFamilyFallback: ['roboto', 'sans-serif'],
        fontSize: 22,
      ),
    );
  }

  static subtitle2(text) {
    return Text(
      text,
      textAlign: TextAlign.center,
      style: TextStyle(
        color: Color.fromRGBO(112, 112, 112, 1),
        fontFamily: 'Segoe UI',
        fontFamilyFallback: ['roboto', 'sans-serif'],
        fontSize: 24,
      ),
    );
  }

  static text(text) {
    return Text(
      text,
      textAlign: TextAlign.center,
      style: TextStyle(
        color: Color.fromRGBO(112, 112, 112, 1),
        fontFamily: 'Segoe UI',
        fontFamilyFallback: ['roboto', 'sans-serif'],
        fontSize: 18,
      ),
    );
  }

  static small(text) {
    return Text(
      text,
      textAlign: TextAlign.center,
      style: TextStyle(
        color: Color.fromRGBO(112, 112, 112, 1),
        fontFamily: 'Segoe UI',
        fontFamilyFallback: ['roboto', 'sans-serif'],
        fontSize: 12,
      ),
    );
  }
}
