import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:motivaapp/model/sign-in.dart';
import 'package:motivaapp/model/Sub.dart';
import 'package:motivaapp/model/user.dart';
import 'package:motivaapp/store.dart';
import 'package:motivaapp/store/application_state.dart';
import 'package:motivaapp/store/set_google_user_action.dart';
import 'package:motivaapp/store/google_user_state.dart';
import 'package:motivaapp/store/user_state.dart';

class AppDrawer extends StatefulWidget {
  AppDrawer(this._parentContext);

  BuildContext _parentContext;
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return AppDrawerState(_parentContext);
  }
}

class AppDrawerState extends State<AppDrawer> implements Sub {
  AppDrawerState(this._parentContext);

  BuildContext _parentContext;

  @override
  notify(account) {
    // TODO: implement notify
    if (account == null) {
      Navigator.pushReplacementNamed(_parentContext, '/');
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    SignIn.addListener(this);
  }

  @override
  void dispose() {
    // TODO: implement dispose
    SignIn.removeListener(this);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    if (ApplicationStore.store.state == null) {
      Navigator.pushReplacementNamed(context, '');
    }
    return Drawer(
      child: ListView(
        children: [
          StoreConnector<ApplicationState, User>(
            converter: (store) => store.state.userState.user,
            builder: (_, user) => UserAccountsDrawerHeader(
              accountName: Text(user?.fullName ?? ''),
              accountEmail: Text(user?.email ?? ''),
              currentAccountPicture: CircleAvatar(
                backgroundImage: NetworkImage(
                    user?.photoUrl ?? 'https://i.imgur.com/mCHMpLT.png'),
              ),
            ),
          ),
          ListTile(
            title: Text('Privacy policy'),
            onTap: () => {},
          ),
          ListTile(
            title: Text('Terms of service'),
            onTap: () => {},
          ),
          Divider(
            color: Colors.black,
            height: 1,
          ),
          ListTile(
            title: Text('Sign out'),
            onTap: () => {SignIn.signIn.signOut()},
          ),
        ],
      ),
    );
  }
}
