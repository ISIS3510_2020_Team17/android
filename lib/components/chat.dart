import 'dart:async';

import 'package:dash_chat/dash_chat.dart';
import 'package:flutter/cupertino.dart';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_dialogflow/dialogflow_v2.dart';
import 'package:motivaapp/components/app_bar.dart';
import 'package:motivaapp/config/app_colors.dart';
import 'package:motivaapp/config/fonts.dart';
import 'package:motivaapp/services/connectivity_check.dart';
import 'package:motivaapp/store.dart';

class Chat extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return ChatState();
  }
}

class ChatState extends State<Chat> {
  Dialogflow bot;
  final GlobalKey<DashChatState> _chatViewKey = GlobalKey<DashChatState>();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  final ChatUser _user = ChatUser(
    name: ApplicationStore.store.state.userState.user.fullName,
    uid: ApplicationStore.store.state.userState.user.id,
    avatar: ApplicationStore.store.state.userState.user.photoUrl,
  );

  final ChatUser _bot = ChatUser(
    name: 'Agent Owl',
    uid: '0',
  );

  ChatState() {
    buildBot();
  }

  void buildBot() async {
    AuthGoogle authGoogle =
        await AuthGoogle(fileJson: 'credentials/owly-292114-fa74f5f13c27.json')
            .build();
    bot = Dialogflow(authGoogle: authGoogle, language: Language.english);
  }

  List<ChatMessage> messages = List<ChatMessage>();

  void receiveBotMessages(message) async {
    AIResponse response = await bot.detectIntent(message.text);
    List<dynamic> responses = response.getListMessage();
    for (final response in responses) {
      final botMessage =
          ChatMessage(text: response['text']['text'][0], user: _bot);
      await FirebaseFirestore.instance
          .collection('users')
          .doc(ApplicationStore.store.state.userState.user.id)
          .collection('messages')
          .doc(DateTime.now().toIso8601String())
          .set(botMessage.toJson());
    }
  }

  void onSend(ChatMessage message, BuildContext context) async {
    if (!await checkConnectivity()) {
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              content: SingleChildScrollView(
                child: ListBody(
                  children: <Widget>[
                    Fonts.title('You don\'t have connection'),
                    Container(height: 24),
                    Fonts.text('Check your connection and try again'),
                  ],
                ),
              ),
              actions: <Widget>[
                TextButton(
                  child: Fonts.text('Ok'),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
              ],
            );
          });
    }
    await FirebaseFirestore.instance
        .collection('users')
        .doc(ApplicationStore.store.state.userState.user.id)
        .collection('messages')
        .doc(DateTime.now().toIso8601String())
        .set(message.toJson());
    receiveBotMessages(message);
    setState(() {
      messages = [...messages, message];
    });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      body: StreamBuilder(
          stream: FirebaseFirestore.instance
              .collection('users')
              .doc(ApplicationStore.store.state.userState.user.id)
              .collection('messages')
              .snapshots(),
          builder: (context, snapshot) {
            if (!snapshot.hasData) {
              return Center(
                child: CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation(
                    AppColors.$accent,
                  ),
                ),
              );
            } else {
              List<DocumentSnapshot> items = snapshot.data.documents;
              List<ChatMessage> messages =
                  items.map((i) => ChatMessage.fromJson(i.data())).toList();
              return DashChat(
                key: _chatViewKey,
                messages: messages,
                user: _user,
                onSend: (send) => onSend(send, context),
                textInputAction: TextInputAction.send,
                inputDecoration:
                    InputDecoration.collapsed(hintText: "Add message here..."),
                dateFormat: DateFormat('yyyy-MMM-dd'),
                timeFormat: DateFormat('HH:mm'),
                showUserAvatar: false,
                showAvatarForEveryMessage: false,
                scrollToBottom: true,
                onPressAvatar: (ChatUser user) {
                  //print("OnPressAvatar: ${user.name}");
                },
                onLongPressAvatar: (ChatUser user) {
                  //print("OnLongPressAvatar: ${user.name}");
                },
                inputMaxLines: 5,
                messageContainerPadding: EdgeInsets.only(left: 5.0, right: 5.0),
                alwaysShowSend: true,
                inputTextStyle: TextStyle(fontSize: 16.0),
                inputContainerStyle: BoxDecoration(
                  border: Border.all(width: 0.0),
                  color: Colors.white,
                ),
                onQuickReply: (Reply reply) {
                  setState(() {
                    messages.add(ChatMessage(
                        text: reply.value,
                        createdAt: DateTime.now(),
                        user: _user));
                    messages = [...messages];
                  });
                  Timer(Duration(milliseconds: 300), () {
                    _chatViewKey.currentState.scrollController
                      ..animateTo(
                        _chatViewKey.currentState.scrollController.position
                            .maxScrollExtent,
                        curve: Curves.easeOut,
                        duration: const Duration(milliseconds: 300),
                      );
                  });
                },
              );
            }
          }),
      appBar: DetailAppBar(context, title: ''),
    );
  }
}
