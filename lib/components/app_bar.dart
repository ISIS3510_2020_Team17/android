import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

DefaultAppBar(context, {title}) => AppBar(
      centerTitle: true,
      title: Title(
        color: Colors.white,
        child: Text(title),
      ),
    );

UserAppBar(context, {title}) => AppBar(
      title: Title(
        color: Colors.white,
        child: Text(title),
      ),
    );

DetailAppBar(context, {title}) => AppBar(
        title: Title(
      color: Colors.white,
      child: Text(title),
    ));
