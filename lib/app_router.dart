import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:motivaapp/components/chat.dart';
import 'package:motivaapp/store.dart';
import 'package:motivaapp/store/google_user_state.dart';

import 'model/sign-in.dart';
import 'pages/Home/home.dart';
import 'pages/Login/login.dart';

class AppRouter {
  static Route<dynamic> onGenerateRoute(settings, context) {
    // This is the router for the app, add your routes here. Routes should go to
    //final user = StoreProvider.of<UserState>(context).state.currentUser;
    final user = ApplicationStore.store.state.userState;
    // User should not be logged of in any component of the app, however if we used another auth method (such as JWT)
    // User should sign in again after a while, this allows app to handle many auth methods without "breaking"
    // changes
    if (user == null) {
      return MaterialPageRoute(builder: (context) => Login());
    }

    if(settings.name == '/home') {
      return MaterialPageRoute(builder: (context) => Home());
    }
    // Define chat in a Generic Way, so having a chat with a professional might be easier to implement
    // TODO: F
    if(settings.name == '/chat/owly') {
      return MaterialPageRoute(builder: (context) => Chat());
    }

    return MaterialPageRoute(builder: (context) => Login());
  }
}
