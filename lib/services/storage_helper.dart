import 'package:localstorage/localstorage.dart';

import '../config/app_routes.dart';

class StorageHelper {
  final LocalStorage localStorage;

  StorageHelper._(this.localStorage);

  factory StorageHelper({route = AppRoutes.DEFAULT_JSON_DIR}) {
    final localStorage = LocalStorage(route);
    return StorageHelper._(localStorage);
  }

  Future<dynamic> getObject(String key) async{
    bool ready = await localStorage.ready;
    if(ready) {
      return await localStorage.getItem(key);
    } else {
      throw Exception('LocalStorage couldn\'t be loaded');
    }
  }

  Future<void> setObject(String key, dynamic value) async{
    bool ready = await localStorage.ready;
    if(ready) {
      return await localStorage.setItem(key, value);
    } else {
      throw Exception('LocalStorage couldn\'t be loaded');
    }
  }

  Future<void> deleteObject(String key) async{
    bool ready = await localStorage.ready;
    if(ready) {
      return await localStorage.deleteItem(key);
    } else {
      throw Exception('LocalStorage couldn\'t be loaded');
    }
  }

}