import 'dart:io';

Future<bool> checkConnectivity() async {
  try {
    final result = await InternetAddress.lookup('www.example.com');
    if(result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
      return true;
    }
    else {
      return false;
    }
  } on SocketException catch(_) {
      return false;
  }
}