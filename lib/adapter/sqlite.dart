import 'dart:async';

import 'package:motivaapp/model/factory.dart';
import 'package:motivaapp/model/jsonifable.dart';
import 'package:motivaapp/model/Sub.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class DBAdapter<Model extends Jsonifable> {
  final String _dbField;

  Model _object;

  Model get object => _object;

  final Sub subscriptor;

  Database _dbconn;

  DBAdapter(this._dbField, this.subscriptor);

  deleteObject(Model object) async {
    if (_dbconn != null && _dbconn.isOpen) {
      await _dbconn
          .delete(_dbField, where: "id = ?", whereArgs: [object.getId()]);
    }
  }

  setObject(Model object) async {
    if (_dbconn != null && _dbconn.isOpen) {
      if ((await _dbconn.query(_dbField)).isEmpty) {
        await _dbconn.insert(_dbField, object.toJson());
      } else {
        await _dbconn.update(_dbField, object.toJson(),
            where: 'id = ?', whereArgs: [object.getId()]);
      }
    }
    _object = object;
  }

  initialize({unique = false}) async {
    _dbconn = await openDatabase(
      // Set the path to the database. Note: Using the `join` function from the
      // `path` package is best practice to ensure the path is correctly
      // constructed for each platform.
      join(await getDatabasesPath(), 'motivapp.db'),
      // When the database is first created, create a table to store dogs.
      onCreate: (db, version) {
        // Run the CREATE TABLE statement on the database.
        return db.execute(
          "CREATE TABLE user(id TEXT PRIMARY KEY, fullName TEXT, photoUrl TEXT, email TEXT)",
        );
      },
      // Set the version. This executes the onCreate function and provides a
      // path to perform database upgrades and downgrades.
      version: 1,
    );
    if (unique) {
      final elements = await _dbconn.query(_dbField);
      if (elements.isNotEmpty) {
        _object = AppFactory.buildFromJson(elements.first);
        if (subscriptor != null) {
          subscriptor.notify(_object);
        }
      }
    }
  }
}
